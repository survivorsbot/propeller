# README #

Christopher Hoang

### What is this repository for? ###

 Propeller is used to control the mobility of the robot.
 We used several sets of motors, an H-bridge, an IR Receiver, and an ultrasonic sensor with the board.


### How do I get set up? ###

 Set up the IR Receiver according to the guideline below. The code used is an edit of the example code given by the SimpleIDE.
 The H-bridge is used to to control and drive the motors. The H-bridge used in this lab needs to at least output 5V. While the example used is for an Arduino board, we used it as an example to wire it to our Propeller board.
 The motors were connected to the H-brige based on the same diagram for the H-bridge.
 The ultrasonic sensor is used as an emergency stop brake for the robot. The code used is an edit of the example code given within the SimpleIDE. Because of the sensor we used having 4Pins, we linked both the echo and trigger pins together in order to be able to use the 3Pin code.
 The Propeller board's multiple cores are essential here as we need all of these functions to happen simultaneously. We used the example in the SimpleIDE as well as an outside reference to better unstand and use the cog functions.
* Configuration
 The app used for the IR Receiver was the AnyMote Smart Remote app found on the Google Play Store. The IR Receiver code example in the SimpleIDE is using a library based off a Sony IR Remote. This app relies on a phone with a built in IR Emitter. After downloading the app, configure a remote as a Sony TV remote. The IR Receiver should now receive commands from the phone if setup correctly.
 Using the multiple cores/cogs of the Propeller, create commands to drive the motors based off of the signal given off by the emitter. The ultrasonic sensor should be configured to kill all motors once a distance threshold has been reached.
 If everything is configured correctly, the robot should move in any given direction by the controller.

### Source Code ###
/*
  Blank Simple Project.c
  http://learn.parallax.com/propeller-c-tutorials 
*/
#include "simpletools.h"                      // Include simple tools
#include "sirc.h"
#include "ping.h"

#define motor1Pin1 0
#define motor1Pin2 1
#define motor2Pin1 2
#define motor2Pin2 3

const int rightButton = 51;
const int leftButton = 52;
const int forwardButton = 116;
const int backwardButton = 117;
const int stopButton = 101;

//Prototypes
void left();
void right();
void forward();
void backward();
void stop();

int main()                                    // Main function
{
  //stop();
  low(motor1Pin1);
  low(motor1Pin2);
  low(motor2Pin1);
  low(motor2Pin2);
 
  while(1)
  {
    print("%c remote button = %d%c",
          HOME, sirc_button(10), CLREOL);
    
    int button = sirc_button(10);
    if(button == rightButton) right();
    if(button == leftButton) left();
    if(button == forwardButton) forward();
    if(button == backwardButton) backward();
    if(button == stopButton) stop();
    
  }  
}
//52
void left(){
    high(motor1Pin1);
    low(motor1Pin2);
  }
//51  
void right(){
    low(motor1Pin1);
    high(motor1Pin2);
  }

//116
void forward(){
    low(motor2Pin1);
    high(motor2Pin2);
  }
//117  
void backward(){
  high(motor2Pin1);
  low(motor2Pin2);
  }
//101
void stop(){
  low(motor1Pin1);
  low(motor1Pin2);
  low(motor2Pin1);
  low(motor2Pin2);
  }
 

### Contribution guidelines ###
* IR Receiver
 https://learn.parallax.com/tutorials/language/propeller-c/propeller-c-simple-devices/ir-receiver-and-remote
* H-bridge
 http://www.instructables.com/id/Arduino-Modules-L298N-Dual-H-Bridge-Motor-Controll/
* Ultrasonic Sensor
 https://learn.parallax.com/tutorials/language/propeller-c/propeller-c-simple-devices/sense-distance-ping
 https://create.arduino.cc/projecthub/microBob/ultra-sonic-ping-sensor-a9c49e
* Multiple Cores
 https://learn.parallax.com/tutorials/language/propeller-c/multicore-approaches/simple-multicore


### Who do I talk to? ###

Christopher Hoang